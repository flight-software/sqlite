cmake_minimum_required(VERSION 3.7)

if(TARGET sqlite)
    return()
endif()

project(sqlite)

include(ExternalProject)

set(CMAKE_C_STANDARD 99)
set(CMAKE_SYSTEM_NAME Linux)

set(DEBUG ON CACHE BOOL "Enable DEBUG build")
set(CROSS_COMPILE OFF CACHE BOOL "Build for flight")

function(set_debug_options debug cross)
    if(${debug})
        if(${cross})
            set(CFLAGS
                -O0
                -g
                -DDEBUG
                "${CFLAGS}"
                PARENT_SCOPE
            )
        else()
            set(CFLAGS
                -Og
                -g
                -DDEBUG
                "${CFLAGS}"
                PARENT_SCOPE
            )
        endif()
    else()
        set(CFLAGS
            -O3
            -Winline
            -DNDEBUG
            "${CFLAGS}"
            PARENT_SCOPE
        )
    endif()
endfunction()

set(FS_LIB_SQLITE_NAME sqlite)
set(FS_LIB_SQLITE_FILE sqlite)

set_debug_options(${DEBUG} ${CROSS_COMPILE})

add_custom_target(${FS_LIB_SQLITE_NAME} ALL) # always run

if(${CROSS_COMPILE})
    add_custom_target(SQLITE_SRC
        COMMAND ./build-cross.sh
        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})
else()
    add_custom_target(SQLITE_SRC
        COMMAND ./build-native.sh
        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})
endif()
add_dependencies(${FS_LIB_SQLITE_NAME} SQLITE_SRC)

if(${CROSS_COMPILE})
    set(CMAKE_SYSTEM_PROCESSOR arm)
    set(CROSS_COMPILER_PREFIX "/usr")
    
    set(CROSS_COMPILER_BINDIR ${CROSS_COMPILER_PREFIX}/bin)
    set(CROSS_COMPILER_INCLUDE ${CROSS_COMPILER_PREFIX}/arm-linux-gnueabihf/include)
    
    set(CMAKE_AR ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ar CACHE FILEPATH "Archiver")
    set(CMAKE_CXX_COMPILER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-g++)
    set(CMAKE_C_COMPILER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-gcc)
    set(CMAKE_LINKER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ld)
    set(CMAKE_NM ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-nm)
    set(CMAKE_OBJCOPY ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-objcopy)
    set(CMAKE_OBJDUMP ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-objdump)
    set(CMAKE_RANLIB ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ranlib CACHE FILEPATH "Runlib")
    set(CMAKE_STRIP ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-strip)

    set(CMAKE_FIND_ROOT_PATH ${CROSS_COMPILER_PREFIX}/arm-linux-gnueabihf)

    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
endif()

