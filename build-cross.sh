#!/bin/bash

test -f built-cross.last && exit 0 || true # only continue if file does not exit
rm -rf built-native.last
rm -rf lib
rm -rf .libs
rm -rf bin
rm -rf includes
make clean
autoreconf --force --install # autoconf is a fucking cancer
./configure --host=arm-linux-gnueabihf --build=x86_64-linux
make -j
mkdir -p lib
mkdir -p bin
mkdir -p includes
cp .libs/libsqlite3.so.0.8.6 lib/libsqlite3.so
cp sqlite3 bin/sqlite3
cp sqlite3.h includes/sqlite3.h
touch built-cross.last
